# Rustup components availability tool

A library and some binaries to monitor rustup components availability history
on different platforms.

## The library part

Please refer to [docs.rs](https://docs.rs/rustup-available-packages) for more info on
the library, and to the source code of the binary crate for usage hints.

## The HTML part

Under the `html` directory you will find a binary crate that's capable of
producing web-pages like
[https://mexus.github.io/rustup-components-history/](https://mexus.github.io/rustup-components-history/).

Run the binary with a `--help` flag to see available options.

More info is coming :)

## The Terminal part

Under the `term` directory you will find a binary crate that you can use
directly from you favourite shell. The `--help` flag is also available.

More info is coming as well :)

### License

Licensed under either of

 * Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

#### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be dual licensed as above, without any
additional terms or conditions.

License: MIT/Apache-2.0
